# seqkit Singularity container
### Bionformatics package seqkit<br>
Cross-platform and ultrafast toolkit for FASTA/Q file manipulation<br>
seqkit Version: 0.12.1<br>
[https://github.com/shenwei356/seqkit]

Singularity container based on the recipe: Singularity.seqkit_v0.12.1

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build seqkit_v0.12.1.sif Singularity.seqkit_v0.12.1`

### Get image help
`singularity run-help ./seqkit_v0.12.1.sif`

#### Default runscript: STAR
#### Usage:
  `seqkit_v0.12.1.sif --help`<br>
    or:<br>
  `singularity exec seqkit_v0.12.1.sif seq --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull seqkit_v0.12.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/seqkit/seqkit:latest`


